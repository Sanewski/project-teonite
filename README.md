# Project Teonite

## 1. Application description
Application is created to gather article information from https://teonite.com/blog/ and save article's author, title and content to Postgresql database. This data is provided to Django based API, which is returning 3 statistics:
* /stats - provides 10 most common words in all articles (excluding stop words),
* /stats/<author_name> - provides 10 most common words in all articles (excluding stop words) created by given author,
* /authors - returns list of all authors and their url_names.

## 2. Database setup
Postgres database is already created. To generate it again clear table and run ```webscrapper/main.py```.

## 3. Local launch
To run app locally run ```python3 manage.pt runserver```

## 3. Docker setup
Before attempting to run ```docker-compose up``` change ```DATABASE``` host to "db" in app/settings.py and engine host to "db" in ```app/engione.py```


