FROM python:3

ENV PYTHONUNBUFFERED=1

WORKDIR /code

COPY requirements.txt /code/

RUN pip3 install --trusted-host pypi.python.org -r requirements.txt

COPY . /code/
