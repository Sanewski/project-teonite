from sqlalchemy.engine import create_engine
from sqlalchemy.orm import sessionmaker

engine = create_engine("postgresql+psycopg2://lukasz:password@127.0.0.1:5432/mydatabase")


def create_session():
    """
    Create session object connected to postgresql database.
    :return: Session
    """
    global engine
    Session = sessionmaker(bind=engine)
    return Session()
