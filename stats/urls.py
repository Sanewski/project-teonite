from django.urls import path
import stats.views

urlpatterns = [
  path('stats/<author_name>', stats.views.AuthorsArticlesCommonWords.as_view()),
  path('authors/', stats.views.AuthorsList.as_view()),
  path('stats/', stats.views.TotalArticlesCommonWords.as_view())
]

