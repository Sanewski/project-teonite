from collections import Counter
from app.engine import create_session
from stats.api_celery import app
from stats.models import Article

db = create_session()


@app.task
def count_words(*args: Article):
    """
    Maps given articles into Counter objects, sums them and returns 10 most common words in final Counter.
    :param args: Article
    :return:
    """
    article_counters_list = map(lambda x: x.clean_article_counter(), args)
    result = sum(article_counters_list, Counter())
    return dict(result.most_common(10))


@app.task()
def authors_list():
    """
    Returns all pairs of article_author_url_name and article_author,from database, as dict with article_author_url_name
    as key and article_author as value.
    :return: dict
    """
    authors_rows = db.query(Article).distinct(Article.article_author_url_name, Article.article_author).all()
    return {row.article_author_url_name: row.article_author for row in authors_rows}

