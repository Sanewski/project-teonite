import re
from collections import Counter

import nltk
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer

from app.engine import engine

Base = declarative_base()

nltk.download('stopwords')
nltk_stopwords = nltk.corpus.stopwords.words('english')


class Article(Base):
    __tablename__ = "articles"

    article_id = Column(Integer, primary_key=True, autoincrement=True)
    article_author = Column(String)
    article_author_url_name = Column(String)
    article_title = Column(String)
    article_content = Column(String)

    def clean_article_counter(self):
        """
        Splits Article object content into list of single words. Removes stopwords from result.
        :return:
        """
        cleared_article = re.findall('[\w]+', self.article_content)
        article_without_stopwords = [word for word in cleared_article if word.lower() not in nltk_stopwords]
        return Counter(article_without_stopwords)

# Run this line to create empty table 'articles' if not existing
# Base.metadata.create_all(engine, checkfirst=True)
