from django.http import JsonResponse, HttpResponse
from rest_framework.views import APIView

from app.engine import create_session
from stats.models import Article
from stats.tasks import count_words, authors_list

db = create_session()


class TotalArticlesCommonWords(APIView):
    """
    List of 10 most common words used in all articles,
    """

    def get(self, request):
        articles = db.query(Article) \
            .all()
        result = count_words.apply(articles)
        return JsonResponse(
            result.get(), status=200
        )


class AuthorsArticlesCommonWords(APIView):
    """
    List of 10 most common words of articles written by specified author.
    """

    def get(self, request, author_name):
        if author_name not in authors_list():
            return JsonResponse(
                {"error": f"{author_name} is not an existing articles author"}, status=204
            )
        articles = db.query(Article) \
            .filter(Article.article_author_url_name == author_name) \
            .all()
        result = count_words.apply(articles)
        return JsonResponse(
            result.get(), status=200
        )


class AuthorsList(APIView):
    """
    List of all authors and their url names.
    """

    def get(self, request):
        authors = authors_list.apply()
        return JsonResponse(
            authors.get(), status=200
        )
